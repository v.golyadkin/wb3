
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>

<body>
    <div class="main">
    <div class="block3">
        <form method="POST" id="form">
            <label>
                Имя:<br />
                <input type=text name="field-name" placeholder="Александер Александров" />
            </label><br />

            <label>
                Поле email:<br />
                <input name="field-email" placeholder="test@example.com" type="email">
            </label><br />

            <label>
                Дата рождения:<br />
                <input name="field-date" value="2000-01-01" type="date" />
            </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-gender" value=1 />
                Мужской</label>
            <label class="radio"><input type="radio" name="radio-gender" value=0 />
                Женский</label><br />

            <label>Кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-limb" value=0 />
                0</label>
            <label class="radio"><input type="radio" name="radio-limb" value=1 />
                1</label>
            <label class="radio"><input type="radio" name="radio-limb" value=2 />
                2</label>
            <label class="radio"><input type="radio" name="radio-limb" value=3 />
                3</label>
            <label class="radio"><input type="radio" name="radio-limb" value=4 />
                4</label><br />

            <label>
                Ваши сверхспособности:<br />
                <select name="superpower" multiple=multiple>
                    <option value="Телекинез">Бессмертие</option>
                    <option value="Смена внешности">Прохождение сквозь стены</option>
                    <option value="Телепортация">Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="BIO" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label>
                <input name="ch" type="checkbox" checked=checked> Ознакомлен с контрактом:<br />
            </label>

            <input type="submit" value="Отправить" />
        </form>
    </div>
</div>
</div>
</body>

</html>

